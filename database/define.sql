/*创建数据库*/
CREATE database bookmanger DEFAULT CHAR SET 'UTF8';
USE bookmanger;

CREATE TABLE tb_bookcase
(
    id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(30)
);
CREATE TABLE tb_manger
(
    id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(30) NOT NULL,
    psw VARCHAR(30) NOT NULL
);
CREATE TABLE tb_parameter
(
    id INT(10) unsigned PRIMARY KEY NOT NULL AUTO_INCREMENT,
    cost INT(10) unsigned,
    validity INT(10) unsigned
);
CREATE TABLE tb_purview
(
    id INT(11) PRIMARY KEY NOT NULL,
    sysset TINYINT(1) DEFAULT '0',
    readerset TINYINT(1) DEFAULT '0',
    bookset TINYINT(1) DEFAULT '0',
    borrowback TINYINT(1) DEFAULT '0',
    sysquery TINYINT(1) DEFAULT '0'
);
CREATE TABLE tb_library
(
    id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    library_name VARCHAR(20) COMMENT '图书馆名',
    curator VARCHAR(20) COMMENT '馆长',
    tel VARCHAR(20) COMMENT '电话',
    address VARCHAR(100) COMMENT '地址',
    email VARCHAR(100),
    url VARCHAR(100),
    createDate DATE COMMENT '建馆日期',
    introduce TEXT COMMENT '简介'
);
CREATE TABLE tb_booktype
(
    id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    typename VARCHAR(30),
    days INT(11) COMMENT '可借天数'
);
CREATE TABLE tb_bookinfo
(
    barcode VARCHAR(30) COMMENT '条形码',
    bookname VARCHAR(70),
    typeid INT(11),
    author VARCHAR(30),
    translator VARCHAR(30),
    ISBN VARCHAR(20) COMMENT '出版社',
    price FLOAT(8,2),
    page INT(11),
    bookcase INT(11),
    storage INT(11),
    inTime DATE,
    del TINYINT(4) COMMENT '是否删除',
    operator VARCHAR(30),
    id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT
);
CREATE TABLE tb_borrow
(
    id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    readerid INT(11),
    bookid INT(11),
    borrowTime DATE,
    backTime DATE,
    operator VARCHAR(30),
    ifback TINYINT(4) DEFAULT '0' COMMENT '是否归还'
);
CREATE TABLE tb_giveback
(
    id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    readerid INT(11),
    bookid INT(11),
    backTime DATE,
    operator VARCHAR(30)
);
CREATE TABLE tb_publish
(
    ISBN VARCHAR(30) PRIMARY KEY NOT NULL,
    pubname VARCHAR(30)
);
CREATE TABLE tb_reader
(
    id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(20),
    sex CHAR(2) DEFAULT '男',
    barcode VARCHAR(30) COMMENT '条形码',
    vocation VARCHAR(50) COMMENT '职员',
    birthday DATE,
    paperType VARCHAR(10) COMMENT '有效证件',
    paperNO VARCHAR(20) COMMENT '证件号码',
    tel VARCHAR(20),
    email VARCHAR(20),
    createDate DATE,
    operator VARCHAR(30),
    remark TEXT COMMENT '备注',
    typeid INT(11) COMMENT '类型'
);
CREATE TABLE tb_readertype
(
    id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(50),
    number INT(11) COMMENT '可借书数量'
);