<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2016/8/25
  Time: 15:43
  To change this template use File | Settings | File Templates.
--%>
<style type="text/css">
    .row input{
        margin-bottom: 20px;
    }
    #submit{
        margin-top: 40px;
    }
</style>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
    request.setAttribute("basePath", basePath);
%>
<div class="panel panel-success">
    <div class="panel-heading">
        <h3 class="panel-title">新增图书</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-12">
                <input id="bookname" type="text" class="form-control" placeholder="图书名">
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6">
                <input id="barcode" type="text" class="form-control" placeholder="条形码">
            </div>
            <div class="col-xs-6">
                <input id="author" type="text" class="form-control" placeholder="作者">
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6">
                <input id="ISBN" type="text" class="form-control" placeholder="出版社">
            </div>
            <div class="col-xs-6">
                <input id="price" type="text" class="form-control" placeholder="价格">
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6">
                <input id="storage" type="text" class="form-control" placeholder="库存数量">
            </div>
            <div class="col-xs-6">
                <input id="page" type="text" class="form-control" placeholder="页码">
            </div>
        </div>
        <div class="row">
            <div class="col-xs-4"></div>
            <div class="">
                <button id="submit" type="button" class="btn btn-success col-xs-4">提交</button>
            </div>
            <div class="col-xs-4"></div>
        </div>
    </div>
</div>

<script>
    $(function () {
//        $("#bookname").blur(function () {
//            if ($("#bookname").val() == ""){
//                alert("请输入图书名");
//                $("#bookname").focus();
//            }
//
//        })
        var submit = $("#submit");

        var bookname = $("#bookname");
        var storage = $("#storage");
//是否允许提交
        function canSubmit() {
            if (bookname.val() == ""){
                alert("请输入图书名");
                bookname.focus();
                return false;
            }
            if (storage.val() == ""){
                alert("请输入数量名");
                storage.focus();
                return false;
            }
            return true;
        }

//        点击提交按钮
        submit.click(function () {
            if (canSubmit()){
                var dialog = "#确定要新增图书吗？";
                //用户是否确定新加图书
                var sure = confirm(dialog);
                if(sure == true){
                    //向服务器提交添加图书数据
                    getToServer();

                }
            }
        });
        //处理服务器返回数据
        function dealGetReturn(data) {
            dialog = "新增图书成功，是否返回到查询页面";
            if(data == 1){
                sure = confirm(dialog);
                if(sure == true){
                    //返回到查询页面
                    $.get("${basePath}manager/manager_book.html",
                            {search: $searchStr,get:1},
                            function (data) {
                                $book_info.html(data);
                            })
                }
            }else if (data == 0){
                alert("新增图书失败，请重试");
            }

        }
        //向服务器提交添加图书数据
        function getToServer() {
            $.get("${basePath}/manager/manager_book.html",
                    {get:2,
                        add:1,
                    bookname:$("#bookname").val(),
                        barcode:$("#barcode").val(),
                        author:$("#author").val(),
                        ISBN:$("#isbn").val(),
                        price:$("#price").val(),
                        storage:$("#storage").val(),
                        page:$("#page").val()
                    },function (data) {
                        dealGetReturn(data);
                    });
        }

    });
</script>