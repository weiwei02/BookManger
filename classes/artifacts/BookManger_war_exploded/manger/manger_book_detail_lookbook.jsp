<%--
  Created by IntelliJ IDEA.
  User: weiwei02
  Date: 2016/8/29
  Time: 17:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
    request.setAttribute("basePath", basePath);
%>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">图书详情信息</h3>
    </div>
    <div class="panel-body">
        <table class="table">
            <caption>${bookinfo.bookname}的详情信息</caption>
            <thead>
            <tr>
                <th>名称</th>
                <th>信息</th>
            </tr>
            </thead>
            <tbody>
            <tr class="active">
                <td>图书名</td>
                <td>
                    <input  name="图书名" uid="0"
                            class="form-control" type="text" value="${bookinfo.bookname}"  readonly>
                </td>
            </tr>
            <tr class="success">
                <td>作者</td>
                <td>
                    <input  name="作者" uid="1"
                            class="form-control" type="text" value="${bookinfo.author}"  readonly>
                </td>
            </tr>
            <tr class="warning">
                <td>条形码</td>
                <td>
                    <input   uid="2"
                            class="form-control" type="text" value="${bookinfo.barcode}"  readonly>
                </td>
            </tr>
            <tr class="danger">
                <td>储量</td>
                <td>
                    <input   uid="3"
                             class="form-control" type="text" value="${bookinfo.storage}"  readonly>
                </td>
            </tr>
            <tr class="active">
                <td>翻译人员</td>
                <td>
                    <input   uid="4"
                             class="form-control" type="text" value="${bookinfo.author}"  readonly>
                </td>
            </tr>
            <tr class="success">
                <td>页数</td>
                <td>
                    <input   uid="5"
                             class="form-control" type="text" value="${bookinfo.page}"  readonly>
                </td>
            </tr>
            <tr class="warning">
                <td>入库时间</td>
                <td>
                    <input   uid="6"
                             class="form-control" type="text" value="${bookinfo.intime}"  readonly>
                </td>
            </tr>
            <tr class="danger">
                <td>操作员</td>
                <td>
                    <input   uid="7"
                             class="form-control" type="text" value="${bookinfo.operator}"  readonly>
                </td>
            </tr>
            <%--图书类型与书架信息--%>
            <tr class="active">
                <td>图书类别</td>
                <td>
                    <input   uid="8"
                             class="form-control " type="text" value="
                    <%if(request.getAttribute("booktype") == null){ %>
                    未知类型
                    <%}else {%>
                    ${booktype.typename}
                    <%}%>"  readonly>
                </td>
            </tr>
            <tr class="success">
                <td>书架</td>
                <td>
                    <input   uid="9"
                             class="form-control" type="text" value="
                    <%if(request.getAttribute("bookcase") == null){ %>
                    未知书架
                    <%}else {%>
                    ${bookcase.name}
                    <%}%>"  readonly>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModal-title">
    <div class="modal-dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModal-title"></h4>
            </div>
            <div class="modal-body">
                <%--面板内容--%>
                <div id="modal-body-input" class="input-group">
                    <span class="input-group-addon" id="userInput_label"></span>
                    <div id="input-panel">
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button id="close" type="button" class="btn btn-default" data-dismiss="modal">关闭</button>

                <button id="sava" class="btn btn-success">保存</button>
                <button id="cancel" class="btn btn-primary">取消</button>
                <button id="add" class="btn btn-info">添加</button>
                <button id="alter" class="btn btn-warning">修改</button>
                <button id="delete" class="btn btn-danger">删除</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script src="${basePath}theme/js/Utils.js"></script>

<script>
    //    var testArray = "['tushuming','zuozhe']";
    //    var tarry = eval("(" + testArray + ")");
    //    console.log(tarry[1]);
    $(function(){
        var Chname = ['图书名','作者','条形码','储量','翻译人员','页数','入库时间','操作员','图书类别','书架'];
        var Enname = ['bookname','author','barcode','storage','translator','page','intime','operator','type','bookcase'];


        //隐藏所有按钮
        function hideAllBtn(){
            $(".btn").hide();
        }

        $(".form-control").click(function(){
            var currenF = $(this);
//            console.log(currenF.val());
//            var $name = currenF.attr("name");
//            var $sysValue = currenF.val();
//            $("#userInput_label").text($name);
//            $("#userInput").attr({name:$name,value:$sysValue});
            callModal(currenF);
        });


        //在#input-panel中添加输入框
        function addTxtUserInput() {
            //加入输入框
            $("#input-panel").html(
                    "<input id=\"userInput\" class=\"form-control\" " +
                    "type=\"text\" aria-describedby=\"userInput_label\">"
            );
        }

        /**为弹出框添加输入框*/
        function addAlterModalInput(alterItem) {
            addTxtUserInput();
            //把原来的属性给输入框
            $("#userInput").val(alterItem.val());
            bindClickOnSaveButton(alterItem);
        }

        /**
         * @description 判断第一个参数是否在第二个参数(数组)中
         * @param userInputvalue 用户输入的数据
         * @param   optionArray 一个数组
         * @returns true    如果userInputvalue在optionArray中
         *          false   不在optionArray中*/
        function isInArray(userInputvalue, optionArray) {
            for (i = 0; i < optionArray.length; i++){
                if (userInputvalue == optionArray[i]){
                    return  true;
                }
            }
            return  false;
        }


        //用户输入的数据是否可以提交
        function isNullUserInput(userInputvalue) {
            if((userInputvalue == "") ||
                    (isInArray(userInputvalue,optionArray))){
                //用户输入的数据不能提交到服务器
                return  true;
            }
            return  false;
        }


        //对新增图书类型或书架请求服务器所返回的数据操作
        function handleAddTypeAndCaseFromResponse(userInputvalue,data,alterItem) {
            if(data == 0){
                $("#myModal-title").text("服务器有误,请联系管理员");
                $("#userInput").focus();
            }else if (data == 1){
                //向数组中添加新元素
                // optionArray.push(userInputvalue);
                optionArray.unshift(userInputvalue);
                setModalSeletor(optionArray,alterItem);
                $("#myModal-title").text("增加成功,请选择");
                $("#cancel").hide();
            }else {
                $("#myModal-title").text("未知错误");
                $("#userInput").focus();
            }
        }


        function cancelAndReturnToSelectModal(alterItem) {
            $("#cancel").show();
            $("#cancel").off();
            $("#cancel").click(function () {
                //点击取消,返回列表选择界面
                $("#cancel").hide();
                $("#myModal-title").text("修改${bookinfo.bookname}的" + Chname[alterItem.attr("uid")] + "信息");
                setModalSeletor(optionArray, alterItem);
            });
        }


        //为添加选项界面中的保存和取消按钮添加事件
        function bindClickOnAddSaveAndCancelButton(alterItem) {
            $("#sava").off();
            $("#sava").click(function(){
                var userInputvalue = $("#userInput").val();
                if(isNullUserInput(userInputvalue)){
                    $("#myModal-title").text("你输入的信息重复,请检查后重新输入");
                }else {
                    $.get("${basePath}manager/alterBook",
                            {get:2,param:Enname[alterItem.attr("uid")],operate:0,
                                value:userInputvalue},function(data){
                                    //对新增图书类型或书架请求服务器所返回的数据操作
                                    handleAddTypeAndCaseFromResponse(userInputvalue,data,alterItem);
                            }
                    );
                }
            });
            cancelAndReturnToSelectModal(alterItem);
        }


        //让 添加按钮 响应事件
        function bindClickOnAddButton(alterItem) {
            $("#add").click(function(){
                addTxtUserInput();
                //为保存按钮设置事件
                $("#add").hide();
                $("#delete").hide();
                $("#alter").hide();
                $("#myModal-title").text("请输入要添加的内容");
                bindClickOnAddSaveAndCancelButton(alterItem);
            });
        }
        var basePath = location.protocol + "//" + location.host + "/book";
        //所有参数可选项数组
        var optionArray = null;

        //构造删除确认界面
        function constructorDeleteSelectOptionModal(alterItem,$option){
            $("#mymodal-title").text("删除" + Chname[alterItem.attr("uid")]);
            $("#input-panel").html("<p>你确定要删除" + Chname[alterItem.attr("uid")]
                + ":" + $option.val() + "吗?");
            //隐藏不必要的按钮
            $("#alter").hide();
            $("#add").hide();
            $("#sava").hide();
        }


        //删除列表中当前被选中的项目
        function handleDeleteSelectOption(alterItem, $option) {
            //构造删除确认界面
            constructorDeleteSelectOptionModal(alterItem,$option);
            //确认删除
            $("#delete").off();
            $("#delete").click(function () {
                $.get("${basePath}manager/alterBook",
                        {get:2,param:Enname[alterItem.attr("uid")],operate:1,
                        value:$option.val()},
                        function(data){
                if(data == 1){
                    //删除成功
//                    $("option").remove(":selected");
//                    cancelAndReturnToSelectModal(alterItem);
                    $("#cancel").off();
                    $("#cancel").hide();
                    setModalSeletor(deleteFromArray(optionArray,$option.val()), alterItem);
                }else if(data == 0){
                    $("#input-panel p").text("有其他图书正在占用当前" + Chname[alterItem.attr("uid")]
                        + ",请取消占用后重试");
                }else {
                    $("#input-panel p").text("未知错误");
                }
            });
            });
            //取消删除
            cancelAndReturnToSelectModal(alterItem);
        }


        //列表页删除按钮
        function bindClickOnDeleteButton(alterItem) {
            $("#delete").off();
            $("#delete").click(function(){
//                $("option").remove(":selected");
                //删除列表中当前被选中的项目
                handleDeleteSelectOption(alterItem,$("option:selected"));
            });
        }

        /**构建修改按钮界面*/
        function constructAlterOptionModal(alterItem){
            $("#sava").hide();
            $("#add").hide();
            $("#delete").hide();
            //拿出取消按钮
            cancelAndReturnToSelectModal(alterItem);
        }




        /**第二次确认修改,
         * 给alter按钮添加点击事件,用户确定要修改*/
        function SubAlterTypeAndCaseToServer(alterItem, val) {
            $("#alter").off();
            $("#alter").click(function () {
                if(isNullUserInput($("#userInput").val())){
                    $("#myModal-title").text("你输入的信息有误,请重新输入");
                }else {
                    //向服务器提交修改类型或书架请求
                    $.get("${basePath}manager/alterBook",
                            {get:2,param:Enname[alterItem.attr("uid")],operate:2,
                                value1:val,value2:$("#userInput").val()},
                            function(data){
                                    if(data == 1){
                                        //修改图书类型成功
                                        $("#alter").off();
                                        $("#cancel").off();
                                        $("#cancel").hide();
                                        setModalSeletor(alterFromArray(optionArray,val,$("#userInput").val()), alterItem);
                                        //模态框的标题替换为原来的标题
                                        $("#myModal-title").text("修改${bookinfo.bookname}的" + Chname[alterItem.attr("uid")] + "信息");
                                    }else {
                                        $("#input-panel p").text("未知错误");
                                    }
                            }
                    );
                }
            });
        }


        //让修改按钮响应事件
        function bindClickOnAlterButton(alterItem){
            $("#alter").off();
            $("#alter").click(function () {
                //构建修改按钮界面
                constructAlterOptionModal(alterItem);
                var $select = $("option:selected");
                //构造修改输入框
                addTxtUserInput();
                //改变提示框的值
                $("#myModal-title").text("将" + Chname[alterItem.attr("uid")] + ":" +
                        $select.val() + "修改为:")
                //修改输入框初始值
                $("#userInput").val($select.val());
                //给alter按钮添加点击事件,用户确定要修改
                SubAlterTypeAndCaseToServer(alterItem,$select.val());

            });
        }


        /**
        //让保存按钮响应事件
         */
        function bindClickOnSaveButton(alterItem){
            $("#sava").off();
            $("#sava").click(function(){
                $.get("${basePath}manager/alterBook",
                        {get:3,param:Enname[alterItem.attr("uid")],
                            value:$("#userInput").val(),bookid:${bookinfo.id}},
                        function(data){
                                if(data == 1){
                                    alterItem.val($("#userInput").val());
                                    $("#close").click();
                                }else {
                                    $("#myModal-title").text("保存失败,请稍后重试")
                                }
                                <%=request.getParameter("bookid")%>
                        }
                );
            });
        }

        //根据服务器回应的信息设置选项列表
        function setModalSeletor(data,alterItem) {
            //把data给全局变量
            optionArray = data;
            var oldValue = alterItem.val();
            $("#input-panel").html("<select id=\"userInput\" class=\"form-control\" " +
                    "aria-describedby=\"userInput_label\">");
            $("#sava").show();
            $("#add").show();
            $("#alter").show();
            $("#delete").show();
            for(i = 0; i < data.length; i++){

                if(data[i] != oldValue){
                    $("#userInput").append("<option value='" + data[i] + "'>" + data[i] + "</option>" );
                }else {
                    $("#userInput").append("<option selected='selected' value='" +
                            data[i] + "'>" + data[i] + "</option>" );
                }
            }
            //让添加按钮响应事件
            bindClickOnAddButton(alterItem);
            //让修改按钮响应事件
            bindClickOnAlterButton(alterItem);
            //让删除按钮响应事件
            bindClickOnDeleteButton(alterItem);
            //让保存按钮响应时间
            bindClickOnSaveButton(alterItem);
        }

        function alterModalSeletor(alterItem) {
            var sendToServerData = {get:1,type:'array',param:Enname[alterItem.attr("uid")],response:true};
            //向服务器发送异步请求
            $.get("manager/alterBook",sendToServerData,function (data) {
                setModalSeletor(eval("(" + data + ")"),alterItem);
            })

        }

        function callModal(alterItem){
            hideAllBtn();
            $("#close").show();
            $("#sava").show();
            var uid = alterItem.attr("uid");
            if(uid > 7){
                //selector修改选项
                alterModalSeletor(alterItem);
            }else if(uid < 8) {
                //可直接修改选项
                addAlterModalInput(alterItem);
            }

            $("#myModal-title").text("修改${bookinfo.bookname}的" + Chname[uid] + "信息");
            $("#userInput_label").text(Chname[uid]);

            $("#myModal").modal();

        }
    });
</script>