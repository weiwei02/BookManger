<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
    request.setAttribute("basePath", basePath);
%>
<!DOCTYPE html>
<html>
<head>
    <script src="${basePath}theme/js/jquery-3.1.0.js"></script>
    <script type="text/javascript">
        $(
                function () {
                    //新增图书按钮
                    $addBook = $("#addBook");
                    //	搜索栏输入框
                    $search = $("#search");
                    //	搜索按钮
                    $search_btn = $("#search_btn");
                    //	显示搜索结果
                    $book_info = $("#book_info");
                    //页面信息
                    $page_info = $("#page_info");
                    //刷新信息
                    $refresh = $("#refresh");
                    $refresh.click(function () {
                        extracted();
                    });
//                    点击新增按钮执行函数
                    $addBook.click(function () {
                        $searchStr = $search.val();
                        $.get("${basePath}manager/manager_book.html",
                                {search: $searchStr,get:2},
                                function (data) {
                                    $book_info.html(data);
                                })
                    });
                    /*ajax提交函数*/
                    function extracted() {
                        $searchStr = $search.val();
                        $.get("${basePath}manager/manager_book.html",
                                {search: $searchStr, get: 1},
                                function (data) {
                                    $book_info.html(data);
                                })
                    }
//                    点击搜索按钮执行函数
                    $search_btn.click(function () {
                        extracted();
                    });
                }
        );
    </script>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <!-- Bootstrap -->
    <title>图书管理系统</title>

    <link href="${basePath }bootstrap/css/bootstrap.min.css"
          rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="//cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Custom styles for this template -->
    <link href="${basePath }theme/starter-template.css" rel="stylesheet">
    <base href="<%=basePath%>">

</head>
<body>
<!-- 导航栏 -->
<nav class="navbar navbar-inverse navbar-fixed-top">
    <jsp:include page="../fragment/navgationg_balck1.jsp"></jsp:include>
</nav>

<!-- 主信息 -->

<article>
    <div class="container ">
        <div class="starter-template ">
            <h1>图书档案管理</h1>
            <p class="lead">在这个简洁的页面里你可以进行：</p>
            <p class="lead">1.对图书类型管理2.图书档案管理，只需要按照书名点击检索就能快速得到你想要的信息</p>
            <p class="lead"></p>
        </div>
        <%-- 图书管理主面板 --%>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">图书档案管理</h3>
            </div>
            <div class="panel-body">

                <%--操作页面 --%>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="input-group">
                            <input id="search" type="text" class="form-control" placeholder="快速检索">
                            <span class="input-group-btn">
									<button id="search_btn" class="btn btn-success" type="button">
										Go! <span class="glyphicon glyphicon-search"
                                                  aria-hidden="true"></span>
									</button>
								</span>
                        </div>
                        <!-- /input-group -->
                    </div>
                    <div class="col-lg-6">
                        <%--新增图书按钮 --%>
                        <button id="addBook" class="btn btn-info" type="button">
                            新增 <span class="glyphicon glyphicon-plus"
                                     aria-hidden="true"></span>
                        </button>
                        <%--刷新按钮 --%>
                        <button id="refresh" class="btn btn-warning" type="button">
                            刷新 <span class="glyphicon glyphicon-refresh"
                                     aria-hidden="true"></span>
                        </button>
                    </div>
                </div>
                    <div class="panel-footer">输入书名快速检索相关图书信息</div>
                    <div id="book_info"></div>
            </div>
        </div>
    </div>
</article>
<!-- /.container -->
<footer>
    <jsp:include page="../fragment/foot.jsp"></jsp:include>
</footer>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="${basePath }bootstrap/js/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="${basePath }bootstrap/js/bootstrap.min.js"></script>
</body>
</html>
