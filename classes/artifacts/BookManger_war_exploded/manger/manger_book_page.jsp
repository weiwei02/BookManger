<%@ page import="cn.hn3l.untils.PageRan" %><%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2016/8/24
  Time: 8:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
    request.setAttribute("basePath", basePath);
    //获取分页对象
    PageRan pageRan = (PageRan) request.getAttribute("pageRan");
    int pageNow = (int) request.getAttribute("pageNow");
    pageRan.intlizate(pageNow);
%>

<ul class="pagination ">
    <%
        //设置当前页前面的页面
    %>
    <% if (pageNow == 1){%>
        <li class="disabled">
     <%}else {%>
          <li>
    <%}%>
     <a  aria-label="Previous">
        <span aria-hidden="true">&laquo;</span>
    </a></li>
    <li><a >首页</a></li>
    <%--<li><a href="#"><span class="sr-only">第<%=pageNow%>页</span></a></li>--%>
    <li><a >当前第<%=pageNow%>页</a></li>
    <li><a >共<%=pageRan.getPageCount()%>页</a></li>
    <%if(pageNow == pageRan.getPageCount()){%>
        <li  class="disabled">
    <%}else {%>
        <li>
    <%}%>
    <a  aria-label="Next">
        <span aria-hidden="true">&raquo;</span>
    </a></li>
</ul>
<script>
    //获取传递页面参数
   <%--function  getPageNow(i){--%>
       <%--if(i == 0){--%>
           <%--return <%=pageRan.getPreviousPage()%>;--%>
       <%--}else if(i == 4) {--%>
           <%--return <%=pageRan.getNextPage()%>;--%>
       <%--}else if(i == 1){--%>
           <%--return 1;--%>
       <%--}else if (i == 3){--%>
           <%--return <%=pageRan.getPageCount()%>;--%>
       <%--}--%>
       <%--return <%=pageNow%>--%>
   <%--}--%>
   <%--function getI(dom) {--%>
       <%--if($(dom) == $ul.eq(0)){--%>
           <%--return <%=pageRan.getPreviousPage()%>;--%>
       <%--}else if($(dom) == $ul.eq(4)) {--%>
           <%--return <%=pageRan.getNextPage()%>;--%>
       <%--}else if($(dom) == $ul.eq(1)){--%>
           <%--return 1;--%>
       <%--}else if ($(dom) == $ul.eq(3)){--%>
           <%--return <%=pageRan.getPageCount()%>;--%>
       <%--}--%>
       <%--return 5;--%>
   <%--}--%>
    $ul = $(".pagination li a");
    $(function () {

        $ul.eq(3).click(
                function () {
                    $.get("${basePath}manager/manager_book.html",
                            {get: 1,  pageNow: <%=pageRan.getPageCount()%>},
                            function (data) {
                                $("#books").html(data);
                            }
                    )
                }
        );
        $ul.eq(4).click(
                function () {
                    $.get("${basePath}manager/manager_book.html",
                            {get: 1,  pageNow: <%=pageRan.getNextPage()%>},
                            function (data) {
                                $("#books").html(data);
                            }
                    )
                }
        );
            $ul.eq(0).click(
                    function () {
                    $.get("${basePath}manager/manager_book.html",
                            {get: 1,  pageNow: <%=pageRan.getPreviousPage()%>},
                            function (data) {
                                $("#books").html(data);
                            }
                    )
                    }
            );
        $ul.eq(1).click(
                function () {
                    $.get("${basePath}manager/manager_book.html",
                            {get: 1,  pageNow: 1},
                            function (data) {
                                $("#books").html(data);
                            }
                    )
                }
        );
//        }

    })
</script>