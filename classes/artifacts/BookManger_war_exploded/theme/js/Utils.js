/**java script常用工具类*/


/**修改数组的内容
 * 将数组内的val替换为val2
 * @param optionArray 要进行修改的数组
 * @param   val 数组内原有的参数
 * @param   val2    在数组内将val替换为val2
 *
 * @return  经过修改后的数组*/
function alterFromArray(optionArrays,val,val2) {
    for (temp in optionArrays){
        if(optionArrays[temp] == val){
            optionArrays[temp] = val2;
        }
    }
    return optionArrays;
}

/**从数组中删除一个元素,并返回一个新数组
 * @param optionArrays  要进行操作的数组
 * @param   $optionval  要进行删除的元素值,要求此元素在数组中
 * */
function deleteFromArray(optionArrays,$optionval){
    var newArray = new Array();
    var i = 0;
    for(tempValue in optionArrays){
        if(optionArrays[tempValue] != $optionval){
            newArray[i++] = optionArrays[tempValue];
        }
    }
    return newArray;
}