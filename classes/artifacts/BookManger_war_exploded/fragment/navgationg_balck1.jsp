<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%--导航栏--%>
<% String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() +
					":" + request.getServerPort()  + path + "/";
	request.setAttribute("basePath", basePath);
					%>
<script src="${basePath }theme/js/jquery-3.1.0.min.js">
<!--

//-->
</script>
<div class="container">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle collapsed"
			data-toggle="collapse" data-target="#navbar" aria-expanded="false"
			aria-controls="navbar">
			<span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span>
			<span class="icon-bar"></span> <span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="#">图书管理系统</a>
	</div>
	<div id="navbar" class="collapse navbar-collapse">
		<ul class="nav navbar-nav">
			<li class="active"><a href="#">主页</a></li>
			<li><a href="#about">借书</a></li>
			<li><a href="#contact">还书</a></li>
			<li><a href="${basePath }manager/manager_book.html">系统查询</a></li>
			<li>
			<a href="#" class="dropdown-toggle" data-toggle="dropdown"
				role="button" aria-haspopup="true" aria-expanded="false">更多
					<span class="caret"></span>
			</a>
				<ul class="dropdown-menu">
					<li><a href="#">馆藏查询</a></li>
					<li><a href="${basePath }manager/manager_book.html">图书管理</a></li>
					<li><a href="#">图书出库</a></li>
					<li><a href="#">用户管理</a></li>
					<li role="separator" class="divider"></li>
					<li><a href="${basePath }sort/rank_borrow_book_frag.jsp">排行榜</a></li>
					
					<li role="separator" class="divider"></li>
					<li><a href="#">关于我们</a></li>
				</ul></li>
		</ul>
		 <ul class="nav navbar-nav navbar-right">
        	<li>
        	<div id="user_info">
        	<%
        	if(session.getAttribute("username") == null){
        		%>
        		<a href="${basePath }login/login.html">登录
        	       <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
        	    </a>
        	<%
        	}else{
        		//当用户已经登录时
        	%>
        	<a href="${basePath }borrow/borrowBook.html">
        			${sessionScope.username }
        	       <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
        	</a>
        	<button id="suser" class="btn btn-danger">注销</button>
        	<%} %>
        	</div>
        	</li>
        </ul>
	</div>
	<!--/.nav-collapse -->
	<script type="text/javascript">
	$("#suser").bind("click",function(e){
		//发送一个注销登录请求，如果注销成功，弹出一个提示框，用户已退出
		//把用户名设置为未登录
		$.ajax("${basePath }login/login.html",{
			//使用get方法
			type: 'GET',
			data: 'exit=1',
			success: function(data,textStatus,jqXHR){
				if(data == 1){
					var str = "<a href=\"${basePath }login/login.html\">登录<span class=\"glyphicon glyphicon-user\" aria-hidden=\"true\"></span></a>";
					$("#user_info").html(str);
					alert("已退出");
					}
				}
			})
		})
	</script>
</div>