# 图书管理系统

> 本系统是河南三联网络技术有限公司实训部实际上课项目，由培训部教师和学生共同完成。
现在贡献出来，可供初学者参考.
   
## 所用技术

> 因教学需要，本系统所使用的各种框架以及依赖都是已经成熟且设计概念完善的流行框架
  
### 前端部分

* Bootsrap前端框架
* JQuery函数库
* HTML5
* Ajax

### 后台部分

> 
* spring MVC
* spring
* JDBC DAO
* JSP
* servlet
* My SQL 5.7
* JUnit4
* Spring-test
* 等,详情见Maven的配置文件
  
## 运行环境

### 本系统在如下环境下开发调试并运行成功

> 
* 操作系统： Windows 7
* JDK 1.8
* Maven 3.0
* Tomcat 8.0
* My SQL 5.7

## 主要功能

1. 
    ### 用户登录
    
    > 
    * 用户输入账户名密码去登录
    * 加减法验证码
    * 登录验证
    * 用户注销
 
    ## 首页效果
    
    ![主页](src/main/WebApp/res/img/screensnop/bookmanger_index.png)

    [登录界面演示](src/main/WebApp/res/img/screensnop/markdown/login.md)

2. 
    ### 图书管理

    > 
    * 新增图书
    * 查看图书详情
    * 修改图书
    * 删除图书
       
    [图书管理界面演示](src/main/WebApp/res/img/screensnop/markdown/manager.md)

    **本部分重点在于前端部分Ajax的使用以及后台数据库的组合操作**
       
## 教学笔记

> 
* 
    [Spring IOC零配置](src/test/java/cn/hn3l/spring/springNoSet.md)
* 
    [Spring AOP教学笔记](src/test/java/cn/hn3l/spring/aop/aop.md)