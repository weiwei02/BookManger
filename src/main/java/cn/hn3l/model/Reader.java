package cn.hn3l.model;

public class Reader {
  private Long id;
  private String name;
  private String sex;
  private String barcode;
  private String vocation;
  private java.sql.Date birthday;
  private String papertype;
  private String paperno;
  private String tel;
  private String email;
  private java.sql.Date createdate;
  private String operator;
  private String remark;
  private Long typeid;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSex() {
    return sex;
  }

  public void setSex(String sex) {
    this.sex = sex;
  }

  public String getBarcode() {
    return barcode;
  }

  public void setBarcode(String barcode) {
    this.barcode = barcode;
  }

  public String getVocation() {
    return vocation;
  }

  public void setVocation(String vocation) {
    this.vocation = vocation;
  }

  public java.sql.Date getBirthday() {
    return birthday;
  }

  public void setBirthday(java.sql.Date birthday) {
    this.birthday = birthday;
  }

  public String getPapertype() {
    return papertype;
  }

  public void setPapertype(String papertype) {
    this.papertype = papertype;
  }

  public String getPaperno() {
    return paperno;
  }

  public void setPaperno(String paperno) {
    this.paperno = paperno;
  }

  public String getTel() {
    return tel;
  }

  public void setTel(String tel) {
    this.tel = tel;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public java.sql.Date getCreatedate() {
    return createdate;
  }

  public void setCreatedate(java.sql.Date createdate) {
    this.createdate = createdate;
  }

  public String getOperator() {
    return operator;
  }

  public void setOperator(String operator) {
    this.operator = operator;
  }

  public String getRemark() {
    return remark;
  }

  public void setRemark(String remark) {
    this.remark = remark;
  }

  public Long getTypeid() {
    return typeid;
  }

  public void setTypeid(Long typeid) {
    this.typeid = typeid;
  }
}
