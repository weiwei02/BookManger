package cn.hn3l.model;

public class Purview {
  private Long id;
  private Long sysset;
  private Long readerset;
  private Long bookset;
  private Long borrowback;
  private Long sysquery;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getSysset() {
    return sysset;
  }

  public void setSysset(Long sysset) {
    this.sysset = sysset;
  }

  public Long getReaderset() {
    return readerset;
  }

  public void setReaderset(Long readerset) {
    this.readerset = readerset;
  }

  public Long getBookset() {
    return bookset;
  }

  public void setBookset(Long bookset) {
    this.bookset = bookset;
  }

  public Long getBorrowback() {
    return borrowback;
  }

  public void setBorrowback(Long borrowback) {
    this.borrowback = borrowback;
  }

  public Long getSysquery() {
    return sysquery;
  }

  public void setSysquery(Long sysquery) {
    this.sysquery = sysquery;
  }
}
