package cn.hn3l.model;

public class Library {
  private Long id;
  private String library_name;
  private String curator;
  private String tel;
  private String address;
  private String email;
  private String url;
  private java.sql.Date createdate;
  private String introduce;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getLibrary_name() {
    return library_name;
  }

  public void setLibrary_name(String library_name) {
    this.library_name = library_name;
  }

  public String getCurator() {
    return curator;
  }

  public void setCurator(String curator) {
    this.curator = curator;
  }

  public String getTel() {
    return tel;
  }

  public void setTel(String tel) {
    this.tel = tel;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public java.sql.Date getCreatedate() {
    return createdate;
  }

  public void setCreatedate(java.sql.Date createdate) {
    this.createdate = createdate;
  }

  public String getIntroduce() {
    return introduce;
  }

  public void setIntroduce(String introduce) {
    this.introduce = introduce;
  }
}
