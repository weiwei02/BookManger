package cn.hn3l.model.diffcult;

import cn.hn3l.model.Bookinfo;
import cn.hn3l.model.Booktype;

import java.io.Serializable;

/**
 * Created by Administrator on 2016/8/24.
 */
public class BookAndType implements Serializable {
    public Bookinfo getBookinfo() {
        return bookinfo;
    }

    public void setBookinfo(Bookinfo bookinfo) {
        this.bookinfo = bookinfo;
    }

    private Bookinfo bookinfo;

    public Booktype getBooktype() {
        return booktype;
    }

    public void setBooktype(Booktype booktype) {
        this.booktype = booktype;
    }

    private Booktype booktype;
}
