package cn.hn3l.model;

import java.io.Serializable;

public class Bookinfo implements Serializable{
  private String barcode;
  private String bookname;
  private Long typeid;
  private String author;
  private String translator;
  private String isbn;
  private Double price;
  private Long page;
  private Long bookcase;
  private Long storage;
  private java.sql.Date intime;
  private Long del;
  private String operator;
  private Long id;

  public String getBarcode() {
    return barcode;
  }

  public void setBarcode(String barcode) {
    this.barcode = barcode;
  }

  public String getBookname() {
    return bookname;
  }

  public void setBookname(String bookname) {
    this.bookname = bookname;
  }

  public Long getTypeid() {
    return typeid;
  }

  public void setTypeid(Long typeid) {
    this.typeid = typeid;
  }

  public String getAuthor() {
    return author;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  public String getTranslator() {
    return translator;
  }

  public void setTranslator(String translator) {
    this.translator = translator;
  }

  public String getIsbn() {
    return isbn;
  }

  public void setIsbn(String isbn) {
    this.isbn = isbn;
  }

  public Double getPrice() {
    return price;
  }

  public void setPrice(Double price) {
    this.price = price;
  }

  public Long getPage() {
    return page;
  }

  public void setPage(Long page) {
    this.page = page;
  }

  public Long getBookcase() {
    return bookcase;
  }

  public void setBookcase(Long bookcase) {
    this.bookcase = bookcase;
  }

  public Long getStorage() {
    return storage;
  }

  public void setStorage(Long storage) {
    this.storage = storage;
  }

  public java.sql.Date getIntime() {
    return intime;
  }

  public void setIntime(java.sql.Date intime) {
    this.intime = intime;
  }

  public Long getDel() {
    return del;
  }

  public void setDel(Long del) {
    this.del = del;
  }

  public String getOperator() {
    return operator;
  }

  public void setOperator(String operator) {
    this.operator = operator;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }
}
