package cn.hn3l.model;

public class Parameter {
  private Long id;
  private Long cost;
  private Long validity;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getCost() {
    return cost;
  }

  public void setCost(Long cost) {
    this.cost = cost;
  }

  public Long getValidity() {
    return validity;
  }

  public void setValidity(Long validity) {
    this.validity = validity;
  }
}
