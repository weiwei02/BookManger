package cn.hn3l.model;

public class Borrow {
  private Long id;
  private Long readerid;
  private Long bookid;
  private java.sql.Date borrowtime;
  private java.sql.Date backtime;
  private String operator;
  private Long ifback;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getReaderid() {
    return readerid;
  }

  public void setReaderid(Long readerid) {
    this.readerid = readerid;
  }

  public Long getBookid() {
    return bookid;
  }

  public void setBookid(Long bookid) {
    this.bookid = bookid;
  }

  public java.sql.Date getBorrowtime() {
    return borrowtime;
  }

  public void setBorrowtime(java.sql.Date borrowtime) {
    this.borrowtime = borrowtime;
  }

  public java.sql.Date getBacktime() {
    return backtime;
  }

  public void setBacktime(java.sql.Date backtime) {
    this.backtime = backtime;
  }

  public String getOperator() {
    return operator;
  }

  public void setOperator(String operator) {
    this.operator = operator;
  }

  public Long getIfback() {
    return ifback;
  }

  public void setIfback(Long ifback) {
    this.ifback = ifback;
  }
}
