package cn.hn3l.model;

public class Giveback {
  private Long id;
  private Long readerid;
  private Long bookid;
  private java.sql.Date backtime;
  private String operator;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getReaderid() {
    return readerid;
  }

  public void setReaderid(Long readerid) {
    this.readerid = readerid;
  }

  public Long getBookid() {
    return bookid;
  }

  public void setBookid(Long bookid) {
    this.bookid = bookid;
  }

  public java.sql.Date getBacktime() {
    return backtime;
  }

  public void setBacktime(java.sql.Date backtime) {
    this.backtime = backtime;
  }

  public String getOperator() {
    return operator;
  }

  public void setOperator(String operator) {
    this.operator = operator;
  }
}
