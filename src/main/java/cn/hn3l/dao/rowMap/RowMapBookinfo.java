package cn.hn3l.dao.rowMap;

import cn.hn3l.model.Bookinfo;
import org.springframework.jdbc.core.RowMapper;

import javax.annotation.Resource;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * 图书信息对象与行列映射类
 * 实现RowMapper接口
 * spring jdbc DAO框架查询对象使用
 * Created by weiwei02 on 2016/8/29.
 */
public class RowMapBookinfo implements RowMapper<Bookinfo> {
    @Resource
    private Bookinfo bookinfo;
    @Override
    public Bookinfo mapRow(ResultSet rs, int rowNum) throws SQLException {
        bookinfo.setBarcode(rs.getString("barcode"));
        bookinfo.setBookname(rs.getString("bookname"));
        bookinfo.setTypeid((long) rs.getInt("typeid"));
        bookinfo.setAuthor(rs.getString("author"));
        bookinfo.setTranslator(rs.getString("translator"));
        bookinfo.setIsbn(rs.getString("ISBN"));
        bookinfo.setPrice(rs.getDouble("price"));
        bookinfo.setPage(rs.getLong("page"));
        bookinfo.setBookcase(rs.getLong("bookcase"));
        bookinfo.setStorage(rs.getLong("storage"));
        bookinfo.setIntime(rs.getDate("inTime"));
        bookinfo.setDel(rs.getLong("del"));
        bookinfo.setOperator(rs.getString("operator"));
        bookinfo.setId(rs.getLong("id"));
        return bookinfo;
    }
}
