package cn.hn3l.dao.rowMap;

import cn.hn3l.model.Bookinfo;
import cn.hn3l.model.Booktype;
import cn.hn3l.model.diffcult.BookAndType;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Administrator on 2016/8/24.
 */
public class RowMapBookAndType implements RowMapper<BookAndType> {
//    private BookAndType bookAndType;
    @Override
    public BookAndType mapRow(ResultSet rs, int rowNum) throws SQLException {
//        bookAndType.getBookinfo().setBarcode(rs.getString("barcode"));
        BookAndType bookAndType = new BookAndType();
        bookAndType.setBookinfo(new Bookinfo());
        bookAndType.setBooktype(new Booktype());
        bookAndType.getBookinfo().setBookname(rs.getString("bookname"));
        bookAndType.getBooktype().setTypename(rs.getString("typename"));
        bookAndType.getBookinfo().setId(rs.getLong("tb_bookinfo.id"));
        bookAndType.getBookinfo().setAuthor(rs.getString("author"));
        bookAndType.getBookinfo().setIsbn(rs.getString("ISBN"));
        bookAndType.getBookinfo().setStorage(rs.getLong("storage"));
        return bookAndType;
    }

    public void setBookAndType(BookAndType bookAndType) {
//        this.bookAndType = bookAndType;
    }
}
