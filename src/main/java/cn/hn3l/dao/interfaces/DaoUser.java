package cn.hn3l.dao.interfaces;

import cn.hn3l.model.User;

/**用户持久化操作接口*/
public interface DaoUser {
	public User read();
	public User read(String userName);
}
