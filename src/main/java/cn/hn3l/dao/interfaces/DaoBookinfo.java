package cn.hn3l.dao.interfaces;

import cn.hn3l.model.Bookinfo;

/**
 * Created by weiwei02 on 2016/8/29.
 */
public interface DaoBookinfo {
    Bookinfo getBookinfo(long bookid);


    /**获取bookcase等于integer的bookinfo的数目
     *
     * @param integer   bookcase
     *
     * @return 满足条件的bookinfo的数目*/
    int getBookCountFromBookcaseId(Integer integer);

    /**指定id和列名,来修改指定列的内容
     * @param bookid 表的id
     * @param param 指定的列名
     * @param value 要修改的值*/
    boolean alterBookinfoByIDAndSignalParamAndValue(String bookid, String param, String value);

    int deleteByID(int i);
}
