package cn.hn3l.dao.interfaces;

/**
 * Created by WangWeiwei on 2016/8/23.
 * 借书持久层操作类
 */
public interface DaoBorrow {

    /**该图书是否在已借出状态且未归还
     * @param  bookid 图书id
     * @return true 图书已经被借出且未归还
     *          false 图书不在已借出状态*/
    boolean bookIsBorrowing(String bookid);
}
