package cn.hn3l.dao.interfaces;

import cn.hn3l.model.Bookinfo;
import cn.hn3l.model.diffcult.BookAndType;

import java.util.List;

/**
 * Created by Administrator on 2016/8/23.
 */
public interface DaoManager {
    List<BookAndType> getBookAndTypeList(String search);

    boolean InsertBook(Bookinfo bookinfo);
}
