package cn.hn3l.dao.interfaces;

import cn.hn3l.model.Booktype;

import java.util.List;

/**
 * Created by weiwei02 on 2016/8/29.
 */
public interface DaoBooktype {
    Booktype getBooktype(Long typeid);

    /**获取所有的图书类型*/
    List<Booktype> getAllBooktype();

    boolean addNewBookType(String value);

    boolean deleteBookType(String value);


    /**将图书类型名为value1的类型的名修改为vlaue2*/
    boolean alterNameFromBooktype(String value1, String value2);


    /**根据图书类型名获取图书的id*/
    int findIdByName(String value);
}
