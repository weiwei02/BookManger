package cn.hn3l.dao.interfaces;

import cn.hn3l.model.Bookcase;

import java.util.List;

/**
 * Created by weiwei02 on 2016/8/29.
 */
public interface DaoBookcase {
    Bookcase getBookcase(Long bookcase);

    List<Bookcase> getAllBookcase();

    boolean addNewBookcase(String value);

    boolean deleteBookcase(String value);

    boolean alterNameFromBookcase(String value1, String value2);

    int findIdByName(String value);
}
