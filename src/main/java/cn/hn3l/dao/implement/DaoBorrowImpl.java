package cn.hn3l.dao.implement;

import cn.hn3l.dao.interfaces.DaoBorrow;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.annotation.Resource;

/**
 * @Author Wang Weiwei
 * @Since 16-9-3
 * @Describe
 */
public class DaoBorrowImpl implements DaoBorrow {

    @Resource
    private JdbcTemplate jdbcTemplate;
    @Override
    public boolean bookIsBorrowing(String bookid) {
        String sql = "SELECT COUNT(id) FROM bookmanger.tb_borrow " +
                "WHERE bookid = ? AND ifback = 0";
        try {
            if (jdbcTemplate.queryForObject(sql, Integer.class, bookid) > 0){
                return true;
            }
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
        return false;
    }
}
