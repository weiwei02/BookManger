package cn.hn3l.dao.implement;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import cn.hn3l.dao.interfaces.DaoUser;
import cn.hn3l.model.User;

import javax.annotation.Resource;

/**用户持久层操作类*/
public class DaoUserImpl implements DaoUser,RowMapper<User> {
	@Resource(name = "user")
	private User user;
	@Resource(name = "user")
	private User reUser ;
	private JdbcTemplate jdbcTemplate;
	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	@Override
	public User read() {
		// TODO Auto-generated method stub
//		user = user.findFirst(subQuery, params)
		return user;
	}
	@Override
	public User read(String userName) {
		// TODO Auto-generated method stub
//		user = new User();
		try {
			/*user = jdbcTemplate.queryForObject(
					"select name,psw from tb_manger where name = ?",*/
			user =  jdbcTemplate.queryForObject(
					"select * from tb_manger where name = ?",
					this, new Object[]{userName});
//				new Object[]{userName},	user.getClass());
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
            System.err.println("ERROR:   " + this.getClass() + "  .read(String) :49 " +
                    "\n" + "用户名不存在于数据库中");
        }
		return user;
	}
	@Override
	public User mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
//			rs.absolute(rowNum);
			
			reUser.setId(rs.getInt("id"));
			reUser.setName(rs.getString("name"));
			reUser.setPsw(rs.getString("psw"));
			return reUser;
	}

}
