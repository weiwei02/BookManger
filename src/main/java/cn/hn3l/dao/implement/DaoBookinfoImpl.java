package cn.hn3l.dao.implement;

import cn.hn3l.dao.interfaces.DaoBookinfo;
import cn.hn3l.model.Bookinfo;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by weiwei02 on 2016/8/29.
 */
public class DaoBookinfoImpl implements DaoBookinfo {
    @Resource
    private JdbcTemplate jdbcTemplate;
    @Override
    public Bookinfo getBookinfo(long bookid) {
        String sql = "select * from tb_bookinfo where id = ?";
        RowMapper<Bookinfo> rowMapper =
                BeanPropertyRowMapper.newInstance(Bookinfo.class);
        List<Bookinfo> list = jdbcTemplate.query(sql, rowMapper,bookid);
        return list.get(0);
    }

    @Override
    public int getBookCountFromBookcaseId(Integer integer) {
        String sql = "SELECT COUNT(*) FROM bookmanger.tb_bookinfo WHERE bookmanger.tb_bookinfo.bookcase = ?";

        return jdbcTemplate.queryForObject(sql, Integer.class, integer);
    }

    @Override
    public boolean alterBookinfoByIDAndSignalParamAndValue(String bookid, String param, String value) {
        String sql = "UPDATE bookmanger.tb_bookinfo SET "+ param +" = ? WHERE id = ?";
        if (jdbcTemplate.update(sql, value, bookid) > 0){
            return true;
        }
        return false;
    }

    @Override
    public int deleteByID(int bookid) {
        try {
            String sql = "DELETE FROM bookmanger.tb_bookinfo WHERE id = ?";
            return jdbcTemplate.update(sql,  bookid);
        }catch (Exception e){
            e.printStackTrace();
            return -1;
        }
    }
}
