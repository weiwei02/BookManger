package cn.hn3l.dao.implement;

import cn.hn3l.dao.interfaces.DaoManager;
import cn.hn3l.dao.rowMap.RowMapBookAndType;
import cn.hn3l.model.Bookinfo;
import cn.hn3l.model.diffcult.BookAndType;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2016/8/24.
 */
public class DaoManagerImpl implements DaoManager {
    private BookAndType bookAndType;
    private JdbcTemplate jdbcTemplate;
    private RowMapBookAndType rowMapBookAndType;
    public void setBookAndType(BookAndType bookAndType) {
    }

    @Override
    public List<BookAndType> getBookAndTypeList(String search) {
        String sql = "SELECT * FROM tb_bookinfo LEFT JOIN tb_booktype" +
                "    ON (tb_bookinfo.typeid = tb_booktype.id)" +
                "    WHERE tb_bookinfo.bookname LIKE ? ;";

        return jdbcTemplate.query(sql , rowMapBookAndType, new Object[] {search});
    }

    @Override
    public boolean InsertBook(Bookinfo bookinfo) {
        String sql =
                "INSERT INTO bookmanger.tb_bookinfo (barcode, bookname," +
                        " typeid, author, translator, ISBN, price, page, " +
                        "bookcase, storage, inTime, del, operator) VALUES " +
                        "(?, ?, ? , ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
        Object[] params = new Object[]{bookinfo.getBarcode(),bookinfo.getBookname(),
                bookinfo.getTypeid(),bookinfo.getAuthor(),bookinfo.getTranslator(),
                bookinfo.getIsbn(),bookinfo.getPrice(),bookinfo.getPage(),bookinfo.getBookcase(),
                bookinfo.getStorage(),new Date(),bookinfo.getDel(),bookinfo.getOperator()};
        try{
            if (jdbcTemplate.update(sql, params) > 0){
                return true;
            }
        }catch (Exception e){
            System.err.println("添加图书失败");
            e.printStackTrace();
        }
        return false;
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void setRowMapBookAndType(RowMapBookAndType rowMapBookAndType) {
        this.rowMapBookAndType = rowMapBookAndType;
    }
}
