package cn.hn3l.untils;

/**
 * @author Created by Wang Weiwei on 2016/8/24.
 *
 *
 * @version 0.2.1-beta
 * @since  0.2.1-beta
 */
public class PageRan {
    public int getPageNow() {
        return pageNow;
    }

    public void setPageNow(int pageNow) {
        this.pageNow = pageNow;
    }

    //当前页
    private int pageNow;

    public int getRowCount() {
        return rowCount;
    }

    //    总行数
    private int rowCount;

    public int getPagePerRow() {
        return pagePerRow;
    }

    //每页显示多少行信息
    private int pagePerRow;

    //当前页的第一行
    private int nowRowNum;

    public void setRowCount(int size) {
        this.rowCount = size;
    }

    public int getNowRowNum() {
        return nowRowNum;
    }

    //设置当前行数
    private void setNowRowNum() {
        nowRowNum = (pageNow - 1) * pagePerRow ;
    }

    public void setPagePerRow(int i) {
        this.pagePerRow = i;
    }

    //总页数
    private int pageCount;

    public int getPageCount() {
        return pageCount;
    }

    private void setPageCount() {
        if((rowCount % pagePerRow) == 0){
            pageCount = rowCount / pagePerRow;
        }else {
            pageCount = rowCount / pagePerRow + 1;
        }
    }

    /**初始化分页相关信息*/
    public void intlizate(int pageNow){
        //设置当前页
        setPageNow(pageNow);
        setNowRowNum();
        setPageCount();
        setPreviousPage();
        setNextPage();
    }

    public void setPreviousPage() {
        if(pageNow == 1){
            previousPage = 1;
        }else {
            previousPage = pageNow - 1;
        }
    }

    private int previousPage;
    /**获取上一页*/
    public int getPreviousPage() {
        return previousPage;
    }

    public int getNextPage() {
        return nextPage;
    }

    public void setNextPage() {
        if(pageNow == pageCount){
            nextPage = pageNow;
        }else {
            nextPage = pageNow + 1;
        }
    }

    //下一页
    private int nextPage;

//    //获取到当前页的第一行
//    private int getPageForRow() {
//    }
}
