package cn.hn3l.untils;

/**
 * @Author Wang Weiwei
 * @Since 16-9-3
 * @Describe    数组操作工具类
 */
public class ArrayUtils {

    /**获取一个String值在String数组中的索引
     * @param params String类型的数组
     * @param param String类型的元素
     *
     * @return i > -1 param在params中的索引
     *         i = -1   param不在params中
     *         i < -1   发生了异常*/
    public static int getIndexFromArray(String[] params,String param){
        try {
            for (int i = 0; i < params.length; i++) {
                String s = params[i];
                if (s.equals(param)){
                    return i;
                }
            }
        }catch (Exception ne){
            ne.printStackTrace();
            return -2;
        }
        return -1;
    }

}
