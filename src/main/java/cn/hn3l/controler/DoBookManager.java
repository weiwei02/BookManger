package cn.hn3l.controler;

import cn.hn3l.model.diffcult.BookAndType;
import cn.hn3l.service.implement.ManagerService;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * Created by Wang Weiwei on 2016/8/23.
 *
 * 图书管理
 图书管理主要实现图书类型设置、图书档案管理等功能
 *@version 0.2.1-bata
 * @since 0.2.1-bata
 */
public class DoBookManager implements Controller {
    public ManagerService getManagerService() {
        return managerService;
    }

    public void setManagerService(ManagerService managerService) {
        this.managerService = managerService;
    }

    private ManagerService managerService;

    public ModelAndView getMav() {
        return mav;
    }

    public void setMav(ModelAndView mav) {
        this.mav = mav;
    }

    private ModelAndView mav;



    @Override
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
        //判断用户登录操作
//        if(request.getSession().getAttribute("username") == null){
//            mav.setViewName("/login/login.html");
//            return mav;
//        }
        String getCode = request.getParameter("get");
        if(getCode != null){

            if(getCode.equals("1")){
//                  当用户搜索数据
                System.out.println("接收到客户端get1请求。。。");
                return doSearch(request, mav);
            }else if (getCode.equals("2")){
//                当用户新增图书
                System.err.print("接收到用户get2请求");
                return doAddBook(request,mav, response);
            }
        }else {
//            用户初次请求获取整个页面
            mav.setViewName("/manger/manager_book.jsp");
            return mav;
        }
        return null;
    }

    private ModelAndView doAddBook(HttpServletRequest request, ModelAndView mav, HttpServletResponse response) {
        //当用户发来新增图书请求
        if(request.getParameter("add") != null){
            //添加图书视图
            if(managerService.addBook(request)){
                return getWriter(response,"1",null);
            }else{
                return getWriter(response,"0",null);
            }
        }

        mav.setViewName("/manger/add_book.jsp");
        return mav;
    }

    /**Response直接向客户回应数据*/
    private ModelAndView getWriter(HttpServletResponse response, String msg, ModelAndView mav) {
        PrintWriter printWriter = null;
        try {
            printWriter = response.getWriter();
            printWriter.write(msg);
            printWriter.flush();

        } catch (IOException e) {
//                e.printStackTrace();
            System.err.println("获取response失败");
        }
        return mav;
    }

    /**为页面设置视图*/
//    public void setPage(HttpServletRequest request, ModelAndView mav) {
//        List<BookAndType> list = (List<BookAndType>) request.getSession().getAttribute("search_result");
//        if (list == null){
//            return ;
//        }
//        String pageNow = request.getParameter("pageNow");
//        if(pageNow == null){
//            mav.addObject("pageNow",1);
//        }else {
//            mav.addObject("pageNow", Integer.parseInt(pageNow));
//        }
//        //设置总行数信息
//        pageRan.setRowCount(list.size());
//        //当前初始行数
////        pageRan.setNowRowNum(Integer.parseInt(pageNow));
//        //设置每页显示行数
//        pageRan.setPagePerRow(10);
//        mav.addObject("pageRan", pageRan);
//    }

    private ModelAndView doSearch(HttpServletRequest request, ModelAndView mav) {
        String search = request.getParameter("search");
        List<BookAndType> lists = null;
        if(search != null){
            if( search.equals("")){
                mav.addObject("search_info","你检索的内容为空");
            }else {
//            把搜索功能交给服务层处理,重新构建list
                lists = managerService.getSearchResult(search);
                request.getSession().setAttribute("search_result",lists);
            }
        }
        returnSearchMav(request, mav, lists);
        mav.setViewName("/manger/manger_book_info.jsp");
        return mav;
    }

    private void returnSearchMav(HttpServletRequest request, ModelAndView mav, List<BookAndType> lists) {
        if(lists == null){
            lists = (List<BookAndType>) request.getSession().getAttribute("search_result");
        }
        if(lists != null){
            mav.addObject("search_info","共检索到 " + lists.size() + " 条相关信息");
//            mav.addObject("search_result",lists);
            managerService.setPage(request, mav);
        }else {
            System.out.println("list为空");
        }
    }

//    public void setPageRan(PageRan pageRan) {
//        this.pageRan = pageRan;
//    }
}
