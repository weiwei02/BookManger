package cn.hn3l.controler;

import cn.hn3l.model.Bookcase;
import cn.hn3l.model.Bookinfo;
import cn.hn3l.model.Booktype;
import cn.hn3l.service.interfaces.DetailService;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * Created by weiwei02 on 2016/8/29.
 * 查看图书详情信息控制层
 */
public class DoDetail implements Controller,ApplicationContextAware{
    public void setMav() {
        this.mav = (ModelAndView) applicationContext.getBean("mav");
    }

    private ModelAndView mav;
    @Resource
    ApplicationContext applicationContext;
    @Resource
    private DetailService detailService;
    @Override
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {

        int bookid = -1 ;
        try{
            setMav();
//            System.out.printf("bookid = " + request.getParameter("bookid"));
            bookid = Integer.parseInt(request.getParameter("bookid"));
        }catch (Exception e){
            System.err.println(DoDetail.class.getName() + "图书id为空");
            e.printStackTrace();
        }
        if (bookid == -1){
            mav.addObject("error_msg", "你输入的图书信息为空");
            mav.setViewName("/error/url_error.jsp");
            return mav;
        }else if(bookid > 0){
            try {
                mav.setViewName("/manger/manger_book_detail.jsp");
                    detailService.setBookDetailInformation(bookid, mav);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return mav;
        }
        mav.addObject("error_msg", "你输入的图书信息有误");
        mav.setViewName("/error/url_error.jsp");
        return mav;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

   /* private void setBookDetailInformation(long bookid, ModelAndView mav) {
        Bookinfo bookinfo = detailService.getBookinfo(bookid);
        Bookcase bookcase = detailService.getBookcase(bookinfo.getBookcase());
        Booktype booktype = detailService.getBooktype(bookinfo.getTypeid());
    }*/
}
