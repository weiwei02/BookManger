package cn.hn3l.controler;

import cn.hn3l.service.interfaces.AlterBookService;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by WangWeiwei on 16-8-31.
 * 修改图书信息控制器
 */
public class DoAlterBook implements Controller {
    @Resource
    private ModelAndView mav;
    @Override
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String get = request.getParameter("get");
        if(get != null){
            if(get.equals("1")){
                return handleGetSelector(request,response);
            }else if(get.equals("2")){
                return handleAddSelector(request,response);
            }else if(get.equals("3")){
                return handleSave(request,response);
            }else if(get.equals("4")){
                return handleDeleteBook(request, response);
            }
        }
        return null;
    }

    /**
     * get号为4
     * 处理删除图书请求
     * 回应1:删除图书
     *  0:正在被占用
     *  其他:未知错误*/
    private ModelAndView handleDeleteBook(HttpServletRequest request, HttpServletResponse response) {
        int code = alterBookService.deleteBook(request.getParameter("bookid"));
        if(code == 1){
            printResponse(response, 1);
        }else if (code == 0){
            printResponse(response, 0);
        }else {
            printResponse(response, -1);
        }
        return null;
    }

    private ModelAndView handleSave(HttpServletRequest request, HttpServletResponse response) {
        try {
//            response.getOutputStream().print(1);
            if(alterBookService.alterBookinfoAttribute(request.getParameter("bookid"), request.getParameter("param"),
                    request.getParameter("value"))){
                printResponse(response,1);
            }else {
                printResponse(response,0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    /**处理编号为2的get请求
     * 该请求的作用是用户想添加新的图书类型或者修改删除已有的图书类型或者书架信息
     * 当服务器回应1代表以上某个操作成功
     * 否则服务器回应0*/
    private ModelAndView handleAddSelector(HttpServletRequest request, HttpServletResponse response) {
        try{
            System.err.println(request.getParameter("param"));
                System.err.println(request.getParameter("operate"));
                switch (Integer.parseInt(request.getParameter("operate"))){
                    //删除某一图书类别
                    case 1:{
                        //operate编号为0 代表要增加图书类别或书架
                        if (alterBookService.deleteOption(request.getParameter("param"), request.getParameter("value"))){
                            printResponse(response,1);
                        }else {
                            printResponse(response,0);
                        }
                        break;
                    }
                    case 0:{
                        //operate编号为0 代表要增加图书类别或书架
                        if (alterBookService.addOption(request.getParameter("param"), request.getParameter("value"))){
                            printResponse(response,1);
                        }else {
                            printResponse(response,0);
                        }
                        break;
                    }
                    case 2:{
//                        printResponse(response,1);
                        //operate编号为2 代表要修改图书类别或书架
                        if (alterBookService.alterOption(request.getParameter("param"), request.getParameter("value1")
                                ,request.getParameter("value2"))){
                            printResponse(response,1);
                        }else {
                            printResponse(response,0);
                        }
                        break;
                    }
                }
                return null;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }


    /**服务器向客户机回应参数
     * @param code 回应编号*/
    private void printResponse(HttpServletResponse response,int code) {
        try {
            response.getOutputStream().print(code);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Resource
    private AlterBookService alterBookService;
    /**用于处理当get号为1的情况:
     * 向服务器请求select列表*/
    private ModelAndView handleGetSelector(HttpServletRequest request, HttpServletResponse response) {
        try {
                if (request.getParameter("type").equals("array")){
                    //从服务层获取json数据
                    String jsArray = alterBookService.getOptionArray(request.getParameter("param"));
                    response.setCharacterEncoding("UTF-8");
                    response.getOutputStream().write(jsArray.getBytes("UTF-8"));
                    response.getOutputStream().flush();
                }
//            }
        }catch (Exception e){
            e.printStackTrace();
            System.err.println("DoAlterBook  get请求号为1,但是没有请求参数");
        }
        return null;
    }

}
