package cn.hn3l.service.implement;

import cn.hn3l.dao.interfaces.DaoManager;
import cn.hn3l.model.Bookinfo;
import cn.hn3l.model.diffcult.BookAndType;
import cn.hn3l.untils.PageRan;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Wang Weiwei on 2016/8/23.
图书管理功能服务层
 */
public class ManagerService implements ApplicationContextAware {
    private DaoManager daoManager;
    private BookAndType bookAndType;
    private ApplicationContext  applicationContext;

    public void setBookinfo() {
        this.bookinfo = (Bookinfo) applicationContext.getBean("bookinfo");
    }

    private Bookinfo bookinfo;
    //分页信息操作类
    private PageRan pageRan;
    public List<BookAndType> getSearchResult(String search)
    {
        //对search进行处理
        search = "%" + search + "%";
        return daoManager.getBookAndTypeList(search);
    }

    /**为页面设置分页信息视图*/
    public void setPage(HttpServletRequest request, ModelAndView mav) {
        List<BookAndType> list = (List<BookAndType>) request.getSession().getAttribute("search_result");
        if (list == null){
            return ;
        }
        String pageNow = request.getParameter("pageNow");
        if(pageNow == null){
            mav.addObject("pageNow",1);
        }else {
            mav.addObject("pageNow", Integer.parseInt(pageNow));
        }
        //设置总行数信息
        pageRan.setRowCount(list.size());
        //当前初始行数
//        pageRan.setNowRowNum(Integer.parseInt(pageNow));
        //设置每页显示行数
        pageRan.setPagePerRow(10);
        mav.addObject("pageRan", pageRan);
    }

    public void setDaoManager(DaoManager daoManager) {
        this.daoManager = daoManager;
    }

    public void setPageRan(PageRan pageRan) {
        this.pageRan = pageRan;
    }

    /**增加图书功能*/
    public boolean addBook(HttpServletRequest request) {
        if(request.getParameter("bookname") != null && request.getParameter("storage") != null){
            setBookinfo();
            bookinfo.setBookname(request.getParameter("bookname"));
            bookinfo.setAuthor(request.getParameter("author"));
            bookinfo.setBarcode(request.getParameter("barcode"));
            bookinfo.setIsbn(request.getParameter("ISBN"));
            long page = 0;
            double price = 0;
            try{
                page = Long.parseLong(request.getParameter("page"));
            }catch (Exception e1){
//                return false;
                page = 0;
            }
            try {
                price = Double.parseDouble(request.getParameter("price"));
            }catch (Exception e2){
//                return false;
                price = 0;
            }
            bookinfo.setPage(page);
            bookinfo.setPrice(price);
            try {
                bookinfo.setOperator(request.getSession().getAttribute("username").toString());
            }catch (Exception as){
                bookinfo.setOperator(null);
            }

            return daoManager.InsertBook(bookinfo);
        }else {
            return false;
        }
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
