package cn.hn3l.service.implement;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;


import cn.hn3l.dao.interfaces.DaoUser;
import cn.hn3l.model.User;

public class LoginService {
	@Resource(name = "user")
	private User inputUser;
	@Resource
	private DaoUser daoUser;

	public boolean isRight(HttpServletRequest request){
		inputUser.setName(request.getParameter("userName"));
		inputUser.setPsw(request.getParameter("passWord"));
		User dataUser = daoUser.read(inputUser.getName());
		if((dataUser != null) &&
				inputUser.getPsw().equals(dataUser.getPsw())){
			return true;
		}
		return false;
	}
}
