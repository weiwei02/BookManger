package cn.hn3l.service.implement;

import cn.hn3l.dao.interfaces.DaoBookcase;
import cn.hn3l.dao.interfaces.DaoBookinfo;
import cn.hn3l.dao.interfaces.DaoBooktype;
import cn.hn3l.dao.interfaces.DaoBorrow;
import cn.hn3l.model.Bookcase;
import cn.hn3l.model.Booktype;
import cn.hn3l.service.interfaces.AlterBookService;
import cn.hn3l.untils.ArrayUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by hyc on 16-8-31.
 */
public class AlterBookServiceImpl implements AlterBookService {
    public void setDaoBookcase(DaoBookcase daoBookcase) {
        this.daoBookcase = daoBookcase;
    }

    public void setDaoBooktype(DaoBooktype daoBooktype) {
        this.daoBooktype = daoBooktype;
    }
    @Resource
    private DaoBookcase daoBookcase;
    @Resource
    private DaoBooktype daoBooktype;
    @Override
    public String getOptionArray(int i) {
        List<String> list = new ArrayList<>();
        if(i == 0){
            List<Booktype>  booktypeList = daoBooktype.getAllBooktype();
            for (Booktype booktype : booktypeList){
                list.add(booktype.getTypename());
            }
        }else if (i == 1){
            //获取所有书架信息
            List<Bookcase> bookcaseList = daoBookcase.getAllBookcase();
            for (Bookcase bookcase : bookcaseList){
                list.add(bookcase.getName());
            }
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("[");
        //临时变量,作为list的指针
        int j;
        for (j = 0; j < list.size(); j++) {
            stringBuffer.append("'" + list.get(j) + "'");
            if (j < list.size() - 1){
                stringBuffer.append(",");
            }
        }
        stringBuffer.append("]");
        return stringBuffer.toString();
    }

    @Override
    public boolean addOption(int i, String value) {
        //新增图书类型
        if (i == 0){
            return daoBooktype.addNewBookType(value);
        }else if (i == 1){
            //新增书架
            return daoBookcase.addNewBookcase(value);
        }
        return false;
    }

    @Override
    public boolean addOption(String param, String value) {

        return addOption(ArrayUtils.getIndexFromArray(params, param), value);
    }

    @Override
    public String getOptionArray(String param) {
        return getOptionArray(ArrayUtils.getIndexFromArray(params,param));
    }

    @Override
    public boolean deleteOption(String param, String value) {
        return deleteOption(ArrayUtils.getIndexFromArray(params, param),value);
    }

    @Override
    public boolean deleteOption(int i, String value) {
        //删除图书类型
        if (i == 0){
            return daoBooktype.deleteBookType(value);
        }else if (i == 1){
            //删除书架
            return daoBookcase.deleteBookcase(value);
        }
        return false;
    }

    @Override
    public boolean alterOption(String param, String value1, String value2) {
        return alterOption(ArrayUtils.getIndexFromArray(params, param), value1, value2);
    }

    @Override
    public boolean alterOption(int param, String value1, String value2) {
        if (param == 0){
            //修改图书类型
            return daoBooktype.alterNameFromBooktype(value1, value2);
        }else if (param == 1){
            //修改书架
            return daoBookcase.alterNameFromBookcase(value1, value2);
        }
        return false;
    }

    @Resource
    private DaoBookinfo daoBookinfo;

    @Override
    public boolean alterBookinfoAttribute(String bookid, String param, String value) {
        int paramId = ArrayUtils.getIndexFromArray(params, param);
        if (paramId < 0){
            return false;
        }else if (paramId == 0){
            //当要修改的是图书类型的时候
            value = String.valueOf(daoBooktype.findIdByName(value));
        }else if   (paramId == 1){
            //保存修改后的图书的书架信息
            value = String.valueOf(daoBookcase.findIdByName(value));
        }
        return daoBookinfo.alterBookinfoByIDAndSignalParamAndValue(bookid, tbParams[paramId], value);
    }

    @Resource
    private DaoBorrow daoBorrow;
    @Override
    /** 回应1:删除图书
     *  0:正在被占用
     *  其他:未知错误*/
    public int deleteBook(String bookid) {
        if (daoBorrow.bookIsBorrowing(bookid)) {
            return 0;
        }else {
            if (daoBookinfo.deleteByID(Integer.parseInt(bookid)) > 0){
                return 1;
            }
        }
        return -1;
    }

}
