package cn.hn3l.service.implement;

import cn.hn3l.dao.interfaces.DaoBookcase;
import cn.hn3l.dao.interfaces.DaoBookinfo;
import cn.hn3l.dao.interfaces.DaoBooktype;
import cn.hn3l.model.Bookcase;
import cn.hn3l.model.Bookinfo;
import cn.hn3l.model.Booktype;
import cn.hn3l.service.interfaces.DetailService;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;

/**
 * Created by weiwei02 on 2016/8/29.
 */
public class DetailServiceImp implements DetailService {
    @Resource(name = "daoBookinfo")
    private DaoBookinfo daoBookinfo;
    @Resource(name = "daoBookcase")
    private DaoBookcase daoBookcase;
    @Resource(name = "daoBooktype")
    private DaoBooktype daoBooktype;
    @Override
    public void setBookDetailInformation(long bookid, ModelAndView mav) {
        Bookinfo bookinfo = null;
        Bookcase bookcase = null;
        Booktype booktype = null;
        try {
            bookinfo = daoBookinfo.getBookinfo(bookid);
            try {
                booktype = daoBooktype.getBooktype(bookinfo.getTypeid());
                bookcase = daoBookcase.getBookcase(bookinfo.getBookcase());
            } catch (Exception e) {
                e.printStackTrace();
            }finally {
                mav.addObject("bookinfo", bookinfo);
                mav.addObject("bookcase", bookcase);
                mav.addObject("booktype", booktype);
            }
        } catch (Exception e) {
            e.printStackTrace();
            mav.addObject("error_msg", "没有相关图书信息");
            mav.setViewName("/error/url_error.jsp");
        }
    }
}
