package cn.hn3l.service.interfaces;

import org.springframework.web.servlet.ModelAndView;

/**
 * 查看图书详情信息接口
 * Created by weiwei02 on 2016/8/29.
 */
public interface DetailService {

    /**查看图书详情信息方法
     * @param bookid    :所要查询的图书编号
     * @param mav   ：本方法将为mav对象添加几个对象对象名分别叫做
     *  bookinfo,bookcase,booktype
     *  注意：三个对象任意一个对象都有可能为null*/
    public void setBookDetailInformation(long bookid, ModelAndView mav);
}
