package cn.hn3l.service.interfaces;

/**
 * Created by hyc on 16-8-31.
 */
public interface AlterBookService {
    public final static String[] params = {"type","bookcase","bookname","author","barcode","storage","translator","page","intime","operator"};
    public final static String[] tbParams = {"typeid","bookcase","bookname",
            "author","barcode","storage","translator","page","inTime","operator"};
    /**获取所有的书架信息或者图书类型信息
     * @param i : 0代表图书类型
     *             1代表书架信息
     * @return 返回一个js数组格式的字符串*/
    String getOptionArray(int i);

    boolean addOption(int i, String value);

    /**
     * 向param所对应数据库表中插入一条数据,名字为value
     * @return true 插入成功
     * @return false    插入失败*/
    boolean addOption(String param, String value);


    /**获取所有的书架信息或者图书类型信息
     * @param param: type代表图书类型
     *               bookcase代表书架
     * @return 返回一个js数组格式的字符串*/
    String getOptionArray(String param);

    /**在与param相关联的表中删除name为value的数据
     * @param param 与表相关的对象名
     * @param value 要删除的行的标识
     *
     * @return true 删除成功*/
    boolean deleteOption(String param, String value);


    boolean deleteOption(int i, String value);


    /**
     * 在与当前param相关的表中将value1修改为value2
     *
     * @param param 与表相关的对象名
     * @param value1    用户要进行修改的原本的值
     * @param value2    用户要进行修改的值
     *
     * @return true 修改成功*/
    boolean alterOption(String param, String value1, String value2);

    boolean alterOption(int param, String value1, String value2);


    boolean alterBookinfoAttribute(String bookid, String param, String value);

    int deleteBook(String bookid);
}
