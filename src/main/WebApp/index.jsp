<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
	request.setAttribute("basePath", basePath);
%>
<!DOCTYPE html>
<html>
<head>

<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
<!-- Bootstrap -->
<title>图书管理系统</title>

<link href="${basePath }bootstrap/css/bootstrap.min.css"
	rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="//cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="//cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<!-- Custom styles for this template -->
<link href="${basePath }theme/starter-template.css" rel="stylesheet">
<base href="<%=basePath%>">

</head>
<body>
	<!-- 导航栏 -->
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<jsp:include page="fragment/navgationg_balck1.jsp"></jsp:include>
	</nav>

	<!-- 主信息 -->

	<div class="container ">
		<div class="jumbotron">
			<h1>JAVA WEB</h1>
			<p>java web教学项目，本系统需要登录才能正常使用</p>
			<p>
				<a class="btn btn-primary btn-lg" href="login/login.html"
					role="button">点击登录</a>
			</p>
		</div>

		<div class="page-header">
			<h1>动态网站开发</h1>
		</div>
		<div id="carousel-example-generic" class="carousel slide"
			data-ride="carousel">
			<ol class="carousel-indicators">
				<li data-target="#carousel-example-generic" data-slide-to="0"
					class="active"></li>
				<li data-target="#carousel-example-generic" data-slide-to="1"></li>
				<li data-target="#carousel-example-generic" data-slide-to="2"></li>
				<li data-target="#carousel-example-generic" data-slide-to="3"></li>
			</ol>
			<div class="carousel-inner" role="listbox">
				<div class="item active">
					<img data-src="res/img/183379.jpg"
						src="res/img/first.jpg"
						alt="First slide">
				</div>
				<div class="item">
					<img src="res/img/second.jpg"
							
						alt="Second slide">
				</div>
				<div class="item">
					<img src="res/img/th.jpg"
							
						alt="3">
				</div>
				<div class="item">
					<img data-src="holder.js/1140x500/auto/#555:#333/text:Third slide"
					src="res/img/fo.jpg"
						alt="Third slide">
				</div>
			</div>
			<a class="left carousel-control" href="#carousel-example-generic"
				role="button" data-slide="prev"> <span
				class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				<span class="sr-only">上一个</span>
			</a> <a class="right carousel-control" href="#carousel-example-generic"
				role="button" data-slide="next"> <span
				class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				<span class="sr-only">下一个</span>
			</a>
		</div>
		<div class="starter-template ">
			<h1>图书管理系统</h1>
			<p class="lead">
				课堂实训项目图书管理系统 <br> 以此项目来巩固课堂上所学的关于JAVA WEB的基础知识并借助此项目进阶到JAVA
				WEB的框架
			</p>
		</div>

	</div>
	<!-- /.container -->
	<footer>
		<jsp:include page="fragment/foot.jsp"></jsp:include>
	</footer>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="${basePath }bootstrap/js/jquery.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="${basePath }bootstrap/js/bootstrap.min.js"></script>
</body>
</html>