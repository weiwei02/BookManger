<%@ page import="java.util.List" %>
<%@ page import="cn.hn3l.model.diffcult.BookAndType" %>
<%@ page import="cn.hn3l.untils.PageRan" %><%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2016/8/24
  Time: 8:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
    request.setAttribute("basePath", basePath);
%>
<div id="books">
<div>${search_info}</div>
<table class="table table-striped">
    <thead>
    <tr>
        <th>书名</th>
        <th>作者</th>
        <th>ISBN</th>
        <th>图书类别</th>
        <th>数量</th>
        <th>操作</th>
    </tr>
    </thead>
    <tbody>

    <%--遍历相关图书信息--%>
    <%
        List<BookAndType> list = (List<BookAndType>) session.getAttribute("search_result");
        //获取分页对象
        PageRan pageRan = (PageRan) request.getAttribute("pageRan");
        int pageNow = (int) request.getAttribute("pageNow");
        pageRan.intlizate(pageNow);
        for (int i = 0; i < pageRan.getPagePerRow(); i ++) {
            int rowNow = i + pageRan.getNowRowNum();
            if(rowNow < pageRan.getRowCount()){
                BookAndType bookAndType = list.get(rowNow);
    %>
    <tr id="<%=bookAndType.getBookinfo().getId()%>">
        <td><%=bookAndType.getBookinfo().getBookname()%></td>
        <td><%=bookAndType.getBookinfo().getAuthor()%></td>
        <td><%=bookAndType.getBookinfo().getIsbn()%></td>
        <td><%=bookAndType.getBooktype().getTypename()%></td>
        <td><%=bookAndType.getBookinfo().getStorage()%></td>
        <td>
            <%--查看详情信息 --%>
            <a href="${basePath}/manager/detail?bookid=<%=bookAndType.getBookinfo().getId()%>"
                    target="_blank" data-toggle="tooltip" title="查看<%=bookAndType.getBookinfo().getBookname()%>的详情信息">
										 <span class="glyphicon glyphicon-zoom-in"
                                               aria-hidden="true"></span>
            </a>&nbsp;
            <%--修改 --%>
            <a href="${basePath}/manager/detail?bookid=<%=bookAndType.getBookinfo().getId()%>"
                target="_blank" data-toggle="tooltip" title="修改<%=bookAndType.getBookinfo().getBookname()%>的信息">
										 <span class="glyphicon glyphicon-pencil"
                                               aria-hidden="true"></span>
            </a>&nbsp;
            <%--删除 --%>
            <a id="<%=bookAndType.getBookinfo().getId()%>" class="delete" href="#" onclick="return false;"
               data-toggle="tooltip" title="删除<%=bookAndType.getBookinfo().getBookname()%>这本书"
               bookname="<%=bookAndType.getBookinfo().getBookname()%>"
            >
										 <span class="glyphicon glyphicon-remove"
                                               aria-hidden="true"></span>
            </a>
        </td>
    </tr>
    <%
        }
        }
    %>
    </tbody>
</table>
    <!-- 模态框（Modal） -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">删除图书</h4>
                </div>
                <div class="modal-body">
                    <!--确认删除界面-->
                    <div id="modal-info">
                        <h3>
                            <span class="label label-info">
                                你确定要删除<strong id="delete_bookname"></strong>这本书吗?
                            </span>
                        </h3>
                    </div>
                    <!--删除错误界面 -->
                    <div id="delete-info">删除错误界面</div>
                </div>
                <div class="modal-footer">
                    <button id="close" type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                    <button id="delete" type="button" class="btn btn-danger">删除</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal -->
    </div>
        <script>
                $(function () {
//        $("#myAlert").hide();
                    $("[data-toggle='tooltip']").tooltip();
                    function deleteFail(fInfo) {
                        $("#delete-info").show();
                        $("#delete-info").text(fInfo);
                        $("#delete").hide();
                    }

                    function btnBindDeleteBook(bookid) {
                        $("#delete").show();
                        $("#delete").off();
                        $("#delete").click(function () {
                            $.get("${basePath}manager/alterBook",
                                    {get:4,bookid:bookid},
                                    function (data) {
                                        console.log("data = " + data);
                                        if (data == 1){
                                            $("#close").click();
                                            $("tr[id="+bookid+"]").hide();

                                        }else if (data == 0){
                                            deleteFail("该图书正处于借出状态,请等待读者归还完毕再进行删除!");
                                        }else {
                                            deleteFail("未知错误,请联系管理员");
                                        }
                                    }
                            )
                        });
                    }




                    $(".delete").click(function(){
                        $("#delete-info").hide();
                        var $thisDelete = $(this);
                        $("#delete_bookname").text($thisDelete.attr("bookname"));
                        //显示模态对话框
                        $("#myModal").modal();
                        btnBindDeleteBook($thisDelete.attr("id"));
                    });
                });

</script>

<%--翻页 --%>
<nav id="page_info" class="text-center">
    <jsp:include page="manger_book_page.jsp" ></jsp:include>
</nav>
</div>

