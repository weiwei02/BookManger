<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
    request.setAttribute("basePath", basePath);
%>
<!DOCTYPE html>
<html>
<head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <!-- Bootstrap -->
    <title>${bookinfo.bookname}详情信息</title>

    <link href="${basePath }bootstrap/css/bootstrap.min.css"
          rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="//cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Custom styles for this template -->
    <link href="${basePath }theme/starter-template.css" rel="stylesheet">
    <base href="<%=basePath%>">

</head>
<body>
<!-- 导航栏 -->
<nav class="navbar navbar-inverse navbar-fixed-top">
    <jsp:include page="../fragment/navgationg_balck1.jsp"></jsp:include>
</nav>

<!-- 主信息 -->

<article>
    <div class="container ">
        <div class="starter-template ">
            <h1>图书管理系统</h1>
            <p class="lead">
                ${bookinfo.bookname}详情信息
                    <br> 你可以在这里查看或修改关于${bookinfo.bookname}的信息
            </p>
        </div>

        <%--查看图书信息详情--%>
        <jsp:include page="manger_book_detail_lookbook.jsp"/>

    </div>
</article>
<!-- /.container -->
<footer>
    <jsp:include page="../fragment/foot.jsp"></jsp:include>
</footer>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="${basePath }bootstrap/js/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="${basePath }bootstrap/js/bootstrap.min.js"></script>
</body>
</html>