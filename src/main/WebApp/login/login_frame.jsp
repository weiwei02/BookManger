<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<% String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() +
					":" + request.getServerPort()  + path + "/";
	request.setAttribute("basePath", basePath);%>
<div class="container">
		<%--action="${basePath }login/login.html" method="post"  --%>
      <form class="form-signin">
        <h2 class="form-signin-heading">请输入账号密码登录</h2>
        
        <div id="error_info" class="alert-danger"></div>
        
        <label for="inputEmail" class="sr-only">用户名</label>
        <input type="text" name="userName" id="inputusername" class="form-control" placeholder="用户名" required autofocus>
        <label for="inputPassword" class="sr-only">密码</label>
        <input type="password" name="passWord" id="inputPassword" class="form-control" placeholder="密码" required>
          <%-- 验证码显示区域 --%>
          <div id="captchaDiv" class="alert alert-info" role="alert">
              <img id="captchaImg" src="${basePath}captcha">
              <div style="float: right;" >
                  看不清？点
                  <br>
                  击图片刷新
              </div>
              </div>
          <br>
        <input type="text" name="captcha" id="captcha" class="form-control" placeholder="验证码" required>
        <div class="checkbox">
          <label>
            <input type="checkbox" value="remember-me"> 保持我的登录状态
          </label>
        </div>
        <button id="submit" class="btn btn-lg btn-primary btn-block" type="button">登录</button>
        <div class="alert alert-warning" role="alert">
        	
        	<strong>没有账号？</strong>
        	<a href="${basePath }login/register.jsp">点击此处注册</a>
      </div>
      </form>
	<script type="text/javascript">
	/**
	 * 异步请求，刷新验证码
	 */
	i = 0;
    $captchaDiv = $("#captchaDiv");
    $captchaDiv.click(function () {
        <%--$.get("${basePath}captcha",function () {--%>

        <%--});--%>

        $("#captchaImg").attr({src:"${basePath}captcha?reflush=" + i++});
        console.log("验证码被点击了");

    })
	/**
	 * 异步请求，提交用户登录请求
	 */
	var errorDiv = $("#error_info");
	errorDiv.hide();
	$("#submit").bind("click",function(){
		//获取用户输入的用户名
		userNames = $("#inputusername").val();
		//获取用户密码
		passWords = $("#inputPassword").val();
//        获取用户输入的验证码
        captchaIn = $("#captcha").val();
		//将用户名密码封装成一个对象
		
		var inputData = {userName:userNames,
					passWord:passWords,
                    captcha:captchaIn
				};
		//使用ajax技术
		$.post("${basePath}login/login.html",inputData,
				function(data, textStatus, jqXHR){
				if(data == 1){
				    errorDiv.text("登录成功，正在跳转...");
                    location.href = "${basePath}";
                }else {
                    errorDiv.html(data);
                }
                    errorDiv.show();
				/* $("body").html(data) */
			});
	})
	</script>
    </div> <!-- /container -->
  