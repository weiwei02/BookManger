package cn.hn3l.test;


import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by Administrator on 2016/8/24.
 * 测试父类
 */
public class SuperSpringContext {
    ApplicationContext context ;

    public SuperSpringContext() {
        this.context = getContext();
    }

    public org.springframework.context.ApplicationContext getContext(){
        return new ClassPathXmlApplicationContext("applicationContext.xml");
    }
}
