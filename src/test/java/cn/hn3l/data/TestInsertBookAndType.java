package cn.hn3l.data;

import cn.hn3l.test.SuperSpringContext;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.Random;

/**
 * Created by Administrator on 2016/8/24.
 */
public class TestInsertBookAndType extends SuperSpringContext {

    public void insertBook(){
        String sql = "INSERT INTO bookmanger.tb_bookinfo (barcode, bookname, " +
                "typeid, author, translator, ISBN, price, page, bookcase, " +
                "storage, inTime, del, operator) VALUES " +
                "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
        JdbcTemplate jdbcTemplate = (JdbcTemplate) super.getContext().getBean("jdbcTemplate");
        for (int i = 0; i < 300; i++) {
            jdbcTemplate.update(sql , new Object[]{ Math.random() * 1000000000,
                    getRandomName(), new Random().nextInt(6) + 1, getRandomName(), null, "1", Math.random() * 10000,
                    Math.random() * 100000,
                    Math.random() * 10000, Math.random() * 1000, null, null, new String("张三")});
        }
        System.out.println("更新成功");
    }

    private String getRandomName() {
        Random random = new Random();
        char[] names = new char[random.nextInt(20)];
        for (int i = 0; i < names.length; i++) {
            int a = 97 + random.nextInt(25);
            names[i] =(char) a;
        }
        return new String(names);
    }
}
