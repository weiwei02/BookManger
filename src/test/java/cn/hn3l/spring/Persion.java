package cn.hn3l.spring;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * @Author Wang Weiwei
 * @Since 16-9-5
 * @Describe
 */

@Component
@Scope(scopeName = "prototype")
public class Persion {
    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    private int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name;

    @PostConstruct
    public void init(){
        System.out.printf(Persion.class + "init()被执行了" );
    }

    @PreDestroy
    public void destroy(){
        System.out.printf(Persion.class + "destroy()被执行了" );
    }
}
