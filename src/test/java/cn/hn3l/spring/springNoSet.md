#spring 零配置

 * Spring一共提供4种注解来标注spring的bean
 * @Component 标注一个普通的Spring bean
 * @Controller 标识一个控制层组件
 * @Service
 * @Repository
 * 使用注解来定义bean的生命周期行为
 * @PostConstruct 用来标识一个bean的初始化的方法
 * @PreDestroy 用来标识一个bean的销毁方法
 * 使用@Scope来定义bean的作用域

---

 * 使用@Lazy指定是否延迟加载
 * 使用@Bean来将一个方法的返回值作为一个bean