package cn.hn3l.spring.aop.bank;

/**
 * @Author Wang Weiwei
 * @Since 16-9-5
 * @Describe
 */
public class BankWithdraw extends BankBase {
    @Override
    protected float yourBussiness(Accountinfo accountinfo, float money) {
        System.out.printf("减少account里面的钱数,返回取出的钱数");
        return 0;
    }
}
