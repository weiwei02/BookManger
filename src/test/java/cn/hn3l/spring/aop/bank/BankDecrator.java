package cn.hn3l.spring.aop.bank;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

/**
 * @Author Wang Weiwei
 * @Since 16-9-5
 * @Describe
 */
public class BankDecrator implements MethodInterceptor {
    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {

        System.out.println("账户验证");
        System.out.println("开启事物");

        //执行商业逻辑
        Object returnValue = invocation.proceed();

        System.out.println("关闭事物");
        return returnValue;
    }
}
