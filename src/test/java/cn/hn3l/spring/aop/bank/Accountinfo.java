package cn.hn3l.spring.aop.bank;

/**
 * @Author Wang Weiwei
 * @Since 16-9-5
 * @Describe
 */
public class Accountinfo {
    public Accountinfo() {
    }

    public Accountinfo(String name,float money) {
        this.name = name;
        this.money = money;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name;

    public float getMoney() {
        return money;
    }

    public void setMoney(float money) {
        this.money = money;
    }

    private float money;
}
