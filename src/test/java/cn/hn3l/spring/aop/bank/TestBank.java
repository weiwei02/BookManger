package cn.hn3l.spring.aop.bank;

import org.junit.Test;

/**
 * @Author Wang Weiwei
 * @Since 16-9-5
 * @Describe
 */
public class TestBank {
    @Test
    public void testBank(){
        BankBase bankBase =new BankWithdraw();
        bankBase.implementMethod(new Accountinfo(),100);
    }
}
