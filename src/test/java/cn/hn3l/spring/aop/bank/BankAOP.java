package cn.hn3l.spring.aop.bank;

import org.junit.Test;
import org.springframework.aop.framework.ProxyFactory;

/**
 * @Author Wang Weiwei
 * @Since 16-9-5
 * @Describe
 */
public class BankAOP {
    @Test
    public void testBankAOP(){
        Accountinfo a1 = new Accountinfo("张三", 1000);

        //目标对象(被代理的对象)
        Bank bank = new Bank();
        //产生一个代理工厂
        ProxyFactory proxyFactory = new ProxyFactory();
        //添加代理工厂的拦截器
        proxyFactory.addAdvice(new BankDecrator());
        //设置目标对象
        proxyFactory.setTarget(bank);

        //获取到一个代理的实例
        Bank bank1 = (Bank) proxyFactory.getProxy();

        bank1.deposit(a1,100);

        bank1.withdraw(a1,100);

        bank1.tranf(a1,a1,100);
    }
}
