package cn.hn3l.spring.aop.bank;

/**
 * @Author Wang Weiwei
 * @Since 16-9-5
 * @Describe
 */
public class Bank {


    public float deposit(Accountinfo accountinfo, float money){

        //增加账户里的钱数,并返回账户里当前的钱数
        System.out.println("存钱");

        return 0;
    }


    public float withdraw(Accountinfo accountinfo, float money){
        //验证account是否为合法用户

        //begin transaction

        //减少账户里的钱数,并返回取出的钱数
        System.out.println("取钱");
        //end transaction
        return 0;
    }

    public float tranf(Accountinfo accountinfo1, Accountinfo accountinfo2, float money){
        System.out.println("转账");
        return  0;
    }
}
