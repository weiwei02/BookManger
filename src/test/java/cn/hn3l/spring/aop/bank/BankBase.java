package cn.hn3l.spring.aop.bank;

/**
 * @Author Wang Weiwei
 * @Since 16-9-5
 * @Describe
 */
public abstract class BankBase {

    public float implementMethod(Accountinfo accountinfo, float money){
        //验证是否为合法用户

        //begin transaction


        //执行商业逻辑
        float result = yourBussiness(accountinfo, money);

        //end transaction

        return result;
    }

    protected abstract float yourBussiness(Accountinfo accountinfo, float money) ;
}
