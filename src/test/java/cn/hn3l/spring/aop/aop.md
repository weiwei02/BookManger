# AOP #

![Spring](http://img4.imgtn.bdimg.com/it/u=4051979429,3207754983&fm=21&gp=0.jpg)

* ##   Aspect Oriented Programming 意为面向切面编程   
---
* ##   AOP其实是GoF设计模式的延续,设计模式追求的是调用者和被调用者之间的解耦,AOP也可以说是这种目标的一种实现
---
* ##   AOP的相关概念:

    1. ###    切面(Aspect)
    
        > 切面是指一个关注点的模块化,这个关注点可能会横切多个对象 
        
    2. ###    连接点(JoinPoint)_
    
        > 连接点指的是在程序执行过程中某个特定的点,比如某个方法调用的时候或者处理异常的时候 
        
    3. ### 通知(Advice)
    
        > 通知是指在切面的某个特定的连接点(JoinPoint)上执行的动作.
        > 通知有各种类型,其中包括around,before,after。 
        
    4. ### 切入点(PointCut)
    
        > 切入点是连接点的断言。通知与一个切入点的表达式关联,并在满足这个切入点的连接点上运行。
        Spring切入点表达式如何与连接点匹配是是AOP的核心:Spring默认使用AspectJ语法
         
    5. ### 引入(Introduction)
    
        > 引入也被称为内部类型声明(Inter-type Declaration)。声明额外的方法或者是某个类型的字段,
        Spring允许引入新的接口(以及一个对应的实现)到任何的被代理的对象。
        
    6. ### 目标对象(Target Object)
    
        > 目标对象是一个或多个切面(Aspect)所通知(Advise)的对象。也叫做被通知(Advised)对象。因为
        > spring AOP是通过运行时代理实现的,这个对象永远是一个被代理(proxied)对象
        
    7. ### AOP代理(AOP Proxy)
    
        > AOP代理是指AOP框架所创建的代理对象,用来实现切面契约(Aspect Contract)(包括通知方法执行
        > 等功能)。在spring中,代理可以是JDK动态代理或者CGLIB代理。
        
    8. ### 织入(Weaving)
    
        > 织入是指把切面(Aspect)连接到其它的应用程序类型或者对象上,并且创建一个被通知(Advised)的对象
        > 这些可以在编译时(如使用AspectJ编译器),类加载时和运行时完成。
        
    9. ### 前置通知(Before Advice)
    
        > 指在某个连接点(JoinPoint)之前执行的通知,但是这个通知不能阻止连接点前的执行(除非它抛出了一个
        > 异常)
        
    10. ### 返回后通知(After Returning Advice)
    
        > 返回后通知是指在某连接点(JoinPoint)正常完成执行后的通知
        
    11. ### 抛出异常后通知(After Throwing Advice)
    
        > 抛出异常后所执行的通知
    12. ### 后通知(After (Finally) Advice)
    
        > 后通知是指当某连接点退出的时候执行的通知(不管是否正常退出)
        
    13. ### 环绕通知(Around Advice)
    
        > 环绕通知是包围一个连接点的通知,这是AOP里最强大的通知类型。
