package cn.hn3l.spring.test;

import cn.hn3l.spring.Persion;
import org.junit.Test;

import javax.annotation.Resource;

/**
 * @Author Wang Weiwei
 * @Since 16-9-5
 * @Describe
 */
public class TestPersion extends TestComponent {
    @Resource
    private Persion persion;

    @Test
    public void testPersionDeclear(){

        persion.setName("小明");
        System.out.println(persion.hashCode());
    }
}
